const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

const { lanzarVentanaLogin } = require("./templates/login");
const { lanzarVentanaRegistro } = require("./templates/registro");
const { lanzarVentanaBookmarks } = require("./templates/bookmarks");
const { lanzarVentanaNuevoBookmark } = require("./templates/nuevobookmark");
const { lanzarVentanaNuevoLabel } = require("./templates/nuevolabel");
const { lanzarVentanaLabels } = require("./templates/verlabels");
const { lanzarVentanaEditarLabel } = require("./templates/modificarlabel");
const { lanzarVentanaEditarBookmark } = require("./templates/modificarbookmark");

// Electron reload en modo desarrollo
// if (process.env.NODE_ENV !== "production") {
//   require("electron-reload")(__dirname, {});
// }

var time = 1000
var ventanaInicial;
var ventanaLogin;
var ventanaRegistro;
var ventanaBookmarks;
var ventanaNuevoBookmark;
var ventanaNuevoLabel;
var ventanaLabels;
var ventanaEditarLabel;
var ventanaEditarBookmark;
var datoslogin = false; //inicializa la variable como falsa, pero en cuanto se logea el usuario se cambia al objeto del mismo


// Ready => Aplicación está lista
app.on("ready", () => {
  // Crea la ventana principal
  ventanaInicial = new BrowserWindow({
    width: 565, //ancho
    height: 320, //alto
    title: "Bienvenido a Bookmark Manager <3",
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });
  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "views/index.html"),
    protocol: "file",
    slashes: true,
  });
  // Crea la ventana principal cargando un HTML
  ventanaInicial.loadURL(urlVentana);
  // Cuando se cierre la ventana principal, se cierra al aplicación
  ventanaInicial.on("closed", () => {
    app.quit();
  });

  //MENUS
  const templateMenu = [
    {
      label: "Iniciar Sesion",
      click() {
        // Crear la ventana de crear tarea
        ventanaLogin = lanzarVentanaLogin();
      }
    },
    {
      label: "Registrarse",
      click() {
        // Crear la ventana de crear tarea
        ventanaRegistro = lanzarVentanaRegistro();
      }
    }
  ];
  // if (process.env.NODE_ENV !== "production") {
  //   templateMenu.push({
  //     label: "DevTools",
  //     submenu: [
  //       {
  //         label: "Mostrar u ocultar Dev Tools",
  //         click(item, focusedWindow) {
  //           focusedWindow.toggleDevTools();
  //         },
  //       },
  //       {
  //         role: "reload",
  //       },
  //     ],
  //   });
  // }

  // Verifica si el SO es Mac
  if (process.platform === "darwin") {
    // Elimina del menu la opción 
    // con el nombre de la app
    templateMenu.unshift({
      label: app.getName(),
    });
  }

  // Carga el menu de un template
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);
  // Asigna el menú creado a la aplicación
  Menu.setApplicationMenu(menuPrincipal);

  //ESPACIO PARA TODOS LOS EVENTOS 
  ipcMain.on("datos-login", (evento, datos) => {
    datoslogin = datos;
    ventanaLogin.close()//cerar la del login
    ventanaLogin = null
    ventanaInicial.hide()//ocultar la del inicio
    ventanaBookmarks = lanzarVentanaBookmarks()//inicia la ventana de bookmarks
    ventanaBookmarks.maximize()//maximizar la ventana
    //tiene que esperar a que se inicie la ventana para mandar los datos
    setTimeout(() => {
      ventanaBookmarks.webContents.send("usuariologeado", datoslogin)
    }, time)
    //en caso de cerrarse bookmarks, se cierra todo
    ventanaBookmarks.on("closed", () => {
      ventanaBookmarks = null;
      ventanaInicial.show()
    });
  });
  //evento usuario registrado
  ipcMain.on("registrado-xd", (evento) => {
    ventanaRegistro.close()
    ventanaRegistro = null
  });
  //evento para abrir ventana de crear bookmark
  ipcMain.on("crear-book", (evento, datos) => {
    ventanaNuevoBookmark = lanzarVentanaNuevoBookmark()
    setTimeout(() => {
      ventanaNuevoBookmark.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //evento para abrir ventana de crear label
  ipcMain.on("crear-label", (evento) => {
    ventanaNuevoLabel = lanzarVentanaNuevoLabel()
    setTimeout(() => {
      ventanaNuevoLabel.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //evento para abrir ventana de crear bookmark
  ipcMain.on("ver-label", (evento, datos) => {
    ventanaLabels = lanzarVentanaLabels()
    setTimeout(() => {
      ventanaLabels.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //evento para cerrar ventana de crear bookmark
  ipcMain.on("bookmark-creado", (evento) => {
    ventanaNuevoBookmark.close()
    ventanaBookmarks.reload()
    setTimeout(() => {
      ventanaBookmarks.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //evento para cerrar ventana de crear label
  ipcMain.on("label-creado", (evento) => {
    ventanaNuevoLabel.close()
    ventanaLabels.reload()
    setTimeout(() => {
      ventanaLabels.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //evento de cerrar sesion
  ipcMain.on("cerrarsesion", (evento) => {
    ventanaBookmarks.close()
    datoslogin = false //vuelve a dejar los datos de login en false
    ventanaInicial.show()
    ventanaInicial.reload()
  });
  //reload bookmarks
  ipcMain.on("reload-bookmarks", (evento) => {
    ventanaBookmarks.reload()
    setTimeout(() => {
      ventanaBookmarks.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //reload labels
  ipcMain.on("reload-labels", (evento) => {
    ventanaLabels.reload()
    setTimeout(() => {
      ventanaLabels.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //abrir ventana modificar label
  ipcMain.on("editar-label", (evento, datos) => {
    let idLabel = datos //recoge el id del label a modificar
    let datoslog = datoslogin //recoge el objeto de usuario activo 
    datoslog.id_label = idLabel //concatena ambas cosas
    console.log(datoslog);
    ventanaEditarLabel = lanzarVentanaEditarLabel()// inicia la ventana de editar label
    setTimeout(() => {
      ventanaEditarLabel.webContents.send("usuariologeado", datoslog) //le manda el objeto concatenado
    }, time)
  });
  //evento label modificado
  ipcMain.on("label-editado", (evento) => {
    ventanaEditarLabel.close()
    ventanaLabels.reload()
    setTimeout(() => {
      ventanaLabels.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
  //abrir ventana modificar bookmark
  ipcMain.on("editar-bookmark", (evento, datos) => {
    let idbookmark = datos //recoge el id del label a modificar
    let datoslog = datoslogin //recoge el objeto de usuario activo 
    datoslog.id_book = idbookmark //concatena ambas cosas
    console.log(datoslog);
    ventanaEditarBookmark = lanzarVentanaEditarBookmark()// inicia la ventana de editar label
    setTimeout(() => {
      ventanaEditarBookmark.webContents.send("usuariologeado", datoslog) //le manda el objeto concatenado
    }, time)
  });
  //evento bookmark modificado 
  ipcMain.on("bookmark-editado", (evento) => {
    ventanaEditarBookmark.close()
    ventanaBookmarks.reload()
    setTimeout(() => {
      ventanaBookmarks.webContents.send("usuariologeado", datoslogin)
    }, time)
  });
})

