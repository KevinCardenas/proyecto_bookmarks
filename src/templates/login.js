const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaLogin() {
  // Crea la ventana para crear una nueva tarea
  let ventanaLogin = new BrowserWindow({
    width: 620,
    height: 380,
    title: "Iniciar sesion",
    resizable: false,
    //The lines below solved the issue
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  // Elimina el menú de la ventana para crear tarea
  ventanaLogin.setMenu(null);

  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "../views/login.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventanaLogin.loadURL(urlVentana);

  // Libera memoria cuando se cierra la ventana
  ventanaLogin.on("closed", () => {
    ventanaLogin = null;
  });

  return ventanaLogin
}

module.exports = { lanzarVentanaLogin }