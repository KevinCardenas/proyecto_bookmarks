const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaBookmarks() {

    let ventanaBookmarks = new BrowserWindow({
      title: "Bookmarks",
      resizable: false,
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false
    }
    });
    ventanaBookmarks.setMenu(null);
  
    // Crea la url de la ventana
    let urlVentana = url.format({
      pathname: path.join(__dirname, "../views/bookmarks.html"),
      protocol: "file",
      slashes: true,
    });
    // Carga el HTML
    ventanaBookmarks.loadURL(urlVentana);

    return ventanaBookmarks
  }

module.exports = {lanzarVentanaBookmarks}