const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaLabels() {

    let ventanaLabels = new BrowserWindow({
      title: "Labels",
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false
    }
    });
    
    ventanaLabels.setMenu(null);
  
    // Crea la url de la ventana
    let urlVentana = url.format({
      pathname: path.join(__dirname, "../views/labels.html"),
      protocol: "file",
      slashes: true,
    });
    // Carga el HTML
    ventanaLabels.loadURL(urlVentana);

    ventanaLabels.on("closed", () => {
        ventanaLabels = null;
      });
    return ventanaLabels
  }

module.exports = {lanzarVentanaLabels}