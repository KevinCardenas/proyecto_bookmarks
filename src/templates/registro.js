const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaRegistro() {
    // Crea la ventana para crear una nueva tarea
    let ventanaRegistro = new BrowserWindow({
      title: "Registro",
      width: 620,
      height: 460,
      //The lines below solved the issue
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false
    }
    });
  
    // Elimina el menú de la ventana para crear tarea
    ventanaRegistro.setMenu(null);
  
    // Crea la url de la ventana
    let urlVentana = url.format({
      pathname: path.join(__dirname, "../views/registrar.html"),
      protocol: "file",
      slashes: true,
    });
    // Carga el HTML
    ventanaRegistro.loadURL(urlVentana);
  
    // Libera memoria cuando se cierra la ventana
    ventanaRegistro.on("closed", () => {
      ventanaRegistro = null;
    })

    return ventanaRegistro
  }

module.exports = {lanzarVentanaRegistro}