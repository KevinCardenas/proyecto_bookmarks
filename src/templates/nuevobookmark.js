const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaNuevoBookmark() {

  let ventanaNuevobook = new BrowserWindow({
    width: 435,
    height: 600,
    title: "Crear bookmark",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });
  ventanaNuevobook.setMenu(null);
  
  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "../views/crearbookmark.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventanaNuevobook.loadURL(urlVentana);

  ventanaNuevobook.on("closed", () => {
    ventanaNuevobook = null;
  });
  return ventanaNuevobook
}

module.exports = { lanzarVentanaNuevoBookmark }