const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaEditarLabel() {

  let ventanaEditarlabel = new BrowserWindow({
    width: 435,
    height: 300,
    title: "Editar Label",
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });
  ventanaEditarlabel.setMenu(null);


  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "../views/editarlabel.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventanaEditarlabel.loadURL(urlVentana);

  ventanaEditarlabel.on("closed", () => {
    ventanaEditarlabel = null;
  });
  return ventanaEditarlabel
}

module.exports = { lanzarVentanaEditarLabel }