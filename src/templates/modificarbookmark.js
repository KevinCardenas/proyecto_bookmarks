const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaEditarBookmark() {

    let ventanaEditarbookmark = new BrowserWindow({
        width: 435,
        height: 700,
        title: "Editar Bookmark",
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });
    ventanaEditarbookmark.setMenu(null);


    // Crea la url de la ventana
    let urlVentana = url.format({
        pathname: path.join(__dirname, "../views/editarbookmark.html"),
        protocol: "file",
        slashes: true,
    });
    // Carga el HTML
    ventanaEditarbookmark.loadURL(urlVentana);

    ventanaEditarbookmark.on("closed", () => {
        ventanaEditarbookmark = null;
    });
    return ventanaEditarbookmark
}

module.exports = { lanzarVentanaEditarBookmark }