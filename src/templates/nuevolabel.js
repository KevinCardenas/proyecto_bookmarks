const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function lanzarVentanaNuevoLabel() {

  let ventanaNuevolabel = new BrowserWindow({
    width: 435,
    height: 300,
    title: "Crear Label",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  });

  ventanaNuevolabel.setMenu(null);
  
  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "../views/crearlabel.html"),
    protocol: "file",
    slashes: true,
  });
  // Carga el HTML
  ventanaNuevolabel.loadURL(urlVentana);

  ventanaNuevolabel.on("closed", () => {
    ventanaNuevolabel = null;
  });
  return ventanaNuevolabel
}

module.exports = { lanzarVentanaNuevoLabel }